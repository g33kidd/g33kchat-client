import {
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
  SET_TOKEN,
  SET_USER,
  SIGNUP_ERROR,
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  VERIFY_REQUEST
} from 'const/auth'

import { CALL_SERVER } from 'const/middleware'
import axios from 'axios'

const signup = data => dispatch => dispatch({
  type: SIGNUP_REQUEST,
  [CALL_SERVER]: {
    route: 'signup',
    data,
    method: 'post',
    type: SIGNUP_SUCCESS,
    error_type: SIGNUP_ERROR
  }
})

const login = data => dispatch => dispatch({
  type: LOGIN_REQUEST,
  [CALL_SERVER]: {
    route: 'auth',
    data,
    method: 'post',
    type: LOGIN_SUCCESS,
    error_type: LOGIN_ERROR
  }
})

const verify = () => dispatch => dispatch({
  type: VERIFY_REQUEST,
  [CALL_SERVER]: {
    route: 'verify',
    data: null,
    method: 'post',
    type: LOGIN_SUCCESS,
    error_type: LOGIN_ERROR
  }
})

const logout = () => dispatch => dispatch({ type: LOGOUT })

export { login, signup, logout, verify }