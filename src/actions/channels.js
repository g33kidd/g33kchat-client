import { CALL_SERVER, JOIN } from 'const/middleware'
import {
  CREATE_CHANNEL,
  CREATE_CHANNEL_ERROR,
  CREATE_CHANNEL_SUCCESS,
  FETCH_CHANNEL,
  FETCH_CHANNELS,
  FETCH_CHANNELS_ERROR,
  FETCH_CHANNELS_SUCCESS,
  FETCH_CHANNEL_ERROR,
  FETCH_CHANNEL_SUCCESS,
  SET_CURRENT
} from 'const/channels'

import listener from 'fn/listeners/channelListener'
import { push } from 'react-router-redux'

const getChannels = () => dispatch => dispatch({
  type: FETCH_CHANNELS,
  [CALL_SERVER]: {
    route: 'channels',
    method: 'get',
    data: null,
    type: FETCH_CHANNELS_SUCCESS,
    error_type: FETCH_CHANNELS_ERROR
  }
})

const getChannel = (id) => dispatch => dispatch({
  type: FETCH_CHANNEL,
  [CALL_SERVER]: {
    route: `channels/${id}`,
    method: 'get',
    data: null,
    type: FETCH_CHANNEL_SUCCESS,
    error_type: FETCH_CHANNEL_ERROR
  }
})

const setCurrent = (id) => dispatch => dispatch({
  type: SET_CURRENT,
  payload: id
})

const createChannel = (data) => dispatch => dispatch({
  type: CREATE_CHANNEL,
  [CALL_SERVER]: {
    route: 'channels',
    method: 'post',
    data,
    after: () => push(`/chat/${data.identifier}`),
    type: CREATE_CHANNEL_SUCCESS,
    error_type: CREATE_CHANNEL_ERROR
  }
})

const listen = (identifier) => dispatch => dispatch({
  type: 'listen',
  [JOIN]: {
    topic: `channel:${identifier}`,
    data: {},
    listener
  }
})

export {
  createChannel,
  getChannels,
  getChannel,
  listen,
  setCurrent
}
