import { CALL_SERVER, CALL_SOCKET } from 'const/middleware'
// import {
//   LOGIN_ERROR,
//   LOGIN_REQUEST,
//   LOGIN_SUCCESS,
//   LOGOUT,
//   SET_TOKEN,
//   SET_USER,
//   SIGNUP_ERROR,
//   SIGNUP_REQUEST,
//   SIGNUP_SUCCESS,
//   VERIFY_REQUEST
// } from 'const/auth'

// TODO: type for request?
const newMessage = ({ topic, data }) => dispatch => dispatch({
  [CALL_SOCKET]: {
    topic,
    event: 'message:create',
    data
  }
})

export { newMessage }