import React, { Component } from 'react'

import ColorHash from 'color-hash'
import { Link } from 'react-router-dom'
import { darken } from 'polished'
import styled from 'styled-components'

const hash = new ColorHash({ lightness: 0.6 });

// NOTE: Boolean prop warning on StyledLink? ¯\_(ツ)_/¯

const Wrapper = styled.div`
  padding: 5px 0;
`
const StyledLink = styled(Link)`
  color: ${props => {
    const color = hash.hex(props.ident)
    return (props.iscurrent) ? darken(0.3, color) : color
  }};
  
  font-size: 16px;
  text-decoration: none;
  transition: all 150ms ease;
  font-weight: ${props => props.iscurrent ? '800' : '500'};

  &::before {
    content: '#';
    padding-right: 3px;
  }

  &:hover {
    color: ${props => darken(0.3, hash.hex(props.ident))};
  }
`

class Channel extends Component {
  render () {
    const { channel, iscurrent } = this.props
    const link = `/chat/${channel.identifier}`
    return (
      <Wrapper>
        <StyledLink iscurrent={iscurrent} to={link} ident={channel.identifier}>{channel.identifier}</StyledLink>
      </Wrapper>
    )
  }
}

export default Channel
