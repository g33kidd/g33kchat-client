import React, { Component } from 'react'

import Channel from './Channel'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 20px;
`
const Header = styled.div`
  font-size: 16px;
  color: #333;
  margin-bottom: 0px;
  text-transform: uppercase;
`

class ChannelList extends Component {
  render () {
    const { channels, raw, current } = this.props

    return (
      <Wrapper>
        <Header>All Channels</Header>
        {channels.map((c, i) => 
          <Channel
            iscurrent={current == raw[c].identifier}
            channel={raw[c]}
            key={i} />
        )}
      </Wrapper>
    )
  }
}

export default ChannelList
