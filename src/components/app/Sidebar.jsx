import React, { Component } from 'react'

import ChannelList from './ChannelList'
import User from './User'
import { connect } from 'react-redux'
import { getChannels } from 'actions/channels'
import { push } from 'react-router-redux'
import styled from 'styled-components'

const Wrapper = styled.div`
  flex: 0 0 350px;
  background: #FFF;
  display: flex;
  flex-direction: column;
`

const Content = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
`

// TODO: Find a place for the channel list, because it's often in the way...

class Sidebar extends Component {
  componentDidMount() {
    this.props.getChannels()
  }

  render () {
    const {
      isLoggedIn,
      user,
      sortedChannels,
      rawChannels,
      currentChannel
    } = this.props

    return (
      <Wrapper>
        <Content>
          {this.props.children}
          {isLoggedIn &&
            <ChannelList
              current={currentChannel}
              channels={sortedChannels}
              raw={rawChannels} />}
        </Content>
        {isLoggedIn &&
          <User
            username={user.username}
            onCreateChannelClick={this.props.createChannel}/>}
      </Wrapper>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    isLoggedIn: state.auth.isLoggedIn,
    user: state.auth.user,
    sortedChannels: state.channels.sortedByName,
    rawChannels: state.channels.raw,
    currentChannel: state.channels.current
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    createChannel: () => dispatch(push('/create-channel')),
    getChannels: () => dispatch(getChannels())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
