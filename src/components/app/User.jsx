import React, { Component } from 'react'
import { Plus } from 'react-feather'
import styled from 'styled-components'

const Wrapper = styled.div`
  background: #566270;
  margin-top: auto;
  color: #E0E3DA;
  padding: 20px;
  font-size: 16px;
  display: flex;
  align-items: center;
`
const Name = styled.div`
  flex: 1;
`
const StyledIcon = styled(Plus)`
  border: 0;
  padding: 5px;
  color: #FFF;
  border-radius: 3px;
  transition: all 150ms ease;
  cursor: pointer;

  &:hover {
    background: #A593E0;
    color: #FFF;
  }
`

class User extends Component {
  render () {
    return (
      <Wrapper>
        <Name>{this.props.username}</Name>
        <StyledIcon onClick={this.props.onCreateChannelClick} />
      </Wrapper>
    )
  }
}

export default User
