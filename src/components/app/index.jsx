import React, { Component } from 'react'

import Auth from 'components/auth'
import Channel from 'components/channel'
import CreateChannel from 'components/create-channel'
import Home from 'components/landing/Home'
import { Route } from 'react-router'
import Sidebar from './Sidebar'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  height: 100%;
`

const AuthTest = (props) => {
  return (
    <p>You must create an account before continuing.</p>
  )
}

class App extends Component {
  render() {
    return (
      <Wrapper>
        <Sidebar>
          <Route path="/create-channel" component={CreateChannel} exact />
          <Route path="/auth" component={Auth} exact />
        </Sidebar>

        <Route path="/" component={Home} exact />
        <Route path="/chat/:identifier" component={Channel} exact />
        <Route path="/auth" component={AuthTest} exact />
      </Wrapper>
    )
  }
}

export default App;
