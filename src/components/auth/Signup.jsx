import { Button, Input, FormWrapper } from 'styles/input'
import React, { Component } from 'react'

class Signup extends Component {
  render() {
    const { isLoading } = this.props
    if (isLoading) {
      return <FormWrapper>Wait... it's loading</FormWrapper>
    } else {
      return this.renderForm()
    }
  }

  renderForm() {
    const { onSubmit, onUpdate, formValues } = this.props

    return (
      <form onSubmit={onSubmit}>
        <FormWrapper>
          <Input type="text" placeholder="Username" value={formValues.username} name="username" onChange={onUpdate} />
          <Input type="text" placeholder="Email Address" value={formValues.email} name="email" onChange={onUpdate} />
          <Input type="password" placeholder="Password" value={formValues.password} name="password" onChange={onUpdate} />

          <Button type="submit">Signup</Button>
        </FormWrapper>
      </form>
    )
  }
}

export default Signup;
