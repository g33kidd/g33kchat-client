import React, { Component } from 'react'
import { login, signup } from 'actions/auth'

import Login from './Login'
import Signup from './Signup'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import styled from 'styled-components'

const Wrapper = styled.div`
  background: #FFF;
  display: flex;
  flex-direction: column;
  width: 500px;
  margin: 0 auto;
  border-radius: 3px;
  margin-top: 30px;
`

const SwitchButton = styled.button`
  background: #6a60a9;
  border: 0;
  color: #FFF;
  padding: 15px 10px;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  font-size: 16px;
  transition: background 150ms ease;
  cursor: pointer;

  &:hover {
    background: #5d5398;
  }
`

// TODO: Handle errors
// TODO: Handle adding token to LocalStorage for persisted authentication.

class Auth extends Component {
  state = {
    page: 'signup',
    signupForm: {
      username: '',
      email: '',
      password: ''
    },
    loginForm: {
      username: '',
      password: ''
    }
  }

  render() {
    const { isLoggedIn } = this.props
    if (isLoggedIn) {
      this.props.redirect()
      return <Wrapper>You are already logged in.</Wrapper>
    } else {
      return this.renderAuth()
    }
  }

  renderAuth() { 
    const { page } = this.state
    return (
      <Wrapper>
        {page === 'signup' && this.renderSignup()}
        {page === 'login' && this.renderLogin()}
        {this.renderSwitchButtons()}
      </Wrapper>
    )
  }

  renderLogin() { 
    return <Login
      formValues={this.state.loginForm}
      onSubmit={this._onLoginSubmit}
      isLoading={this.props.loginLoading}
      onUpdate={(e) => this._onFormUpdate(e, 'loginForm')} />
  }

  renderSignup() { 
    return <Signup
      formValues={this.state.signupForm}
      onSubmit={this._onSignupSubmit}
      isLoading={this.props.signupLoading}
      onUpdate={(e) => this._onFormUpdate(e, 'signupForm')} />
  }

  renderSwitchButtons() {
    const { page } = this.state
    const statement = (page === 'signup')
      ? 'Login to my account'
      : 'Create an account'
    return <SwitchButton onClick={this._switchPage}>{statement}</SwitchButton>
  }

  _switchPage = () => {
    const { page } = this.state
    if (page === 'signup') {
      this.setState({ page: 'login' })
    } else {
      this.setState({ page: 'signup' })
    }
  }

  _onLoginSubmit = (e) => {
    e.preventDefault()
    this.props.login(this.state.loginForm)
  }

  _onSignupSubmit = (e) => {
    e.preventDefault()
    this.props.signup(this.state.signupForm)
  }

  // Handles form changes and persists state so user can change between without losing information.
  _onFormUpdate = (e, form) => {
    this.setState({
      [form]: {
        ...this.state[form],
        [e.target.name]: e.target.value
      }
    })
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.isLoggedIn,
    signupLoading: state.auth.signupLoading,
    loginLoading: state.auth.loginLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (data) => dispatch(login(data)),
    signup: (data) => dispatch(signup(data)),
    redirect: () => dispatch(push('/'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth)