import { Button, FormWrapper, Input } from 'styles/input'
import React, { Component } from 'react'
import { getChannel, getChannels, listen, setCurrent } from 'actions/channels'

import { connect } from 'react-redux'
import { newMessage } from 'actions/events'
import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 10px;
`

// TODO: Do something for when channel doesn't actually exist on the server
// but it might still try to connect to the socket. Don't want unecessary socket connections.
// ¯\_(ツ)_/¯

class Channel extends Component {
  componentDidMount() {
    const { identifier } = this.props.match.params
    this.props.setCurrent(this.props.match.params.identifier)
    this.props.listen(identifier)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps === undefined) {
      return false
    }

    if (prevProps.match.params.identifier != this.props.match.params.identifier) {
      this.props.listen(this.props.match.params.identifier)
      this.props.setCurrent(this.props.match.params.identifier)
    }
  }

  state = {
    message: ''
  }

  // TODO: Create a composer component for creating messages... ¯\_(ツ)_/¯
  render () {
    const { channel, events } = this.props
    if (channel) {
      return (
        <Wrapper>
          {events != undefined && events.map(e => <p>{e.message}</p>)}
          <form onSubmit={this._newMessage}>
            <FormWrapper>
              <Input value={this.state.message} onChange={this._onMessageChange} placeholder="Start typing..." />
              <Button type="submit">Create Message</Button>
            </FormWrapper>
          </form>
        </Wrapper>
      )
    } else {
      return "Channel not found! OMG!"
    }
  }

  _newMessage = (e) => {
    e.preventDefault()
    this.props.newMessage({
      topic: `channel:${this.props.match.params.identifier}`,
      data: this.state
    })
  }

  _onMessageChange = (e) => {
    this.setState({
      message: e.target.value
    })
  }
}

const mapStateToProps = (state, ownProps) => {
  let { identifier } = ownProps.match.params
  let channel = state.channels.raw[identifier]
  let events = state.events[identifier]
  return { channel, events }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getChannels: () => dispatch(getChannels()),
    getChannel: (ident) => dispatch(getChannel(ident)),
    setCurrent: (ident) => dispatch(setCurrent(ident)),
    listen: (ident) => dispatch(listen(ident)),
    newMessage: (data) => dispatch(newMessage(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Channel)
