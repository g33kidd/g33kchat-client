import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createChannel } from 'actions/channels'
import { FormWrapper, FormHeader, Input, Button } from 'styles/input'

// TODO: create a validator for channel identifiers
// You can only have [a-z0-9]- as a channel identifier.

class CreateChannel extends Component {
  state = {
    name: '',
    description: '',
    identifier: ''
  }

  render () {
    const { name, description, identifier } = this.state
    const identValue = `#${identifier}`
    return (
      <form onSubmit={this._onSubmit}>
        <FormWrapper>
          <FormHeader>SOME FUN PHRASE</FormHeader>

          <Input type="text" placeholder="Channel Name" value={name} name="name" onChange={this._onUpdate} />
          <Input type="text" placeholder="Description" value={description} name="description" onChange={this._onUpdate} />
          <Input type="text" placeholder="Identifier" value={identifier} name="identifier" onChange={this._onUpdate} />

          <Button type="submit">Create Channel</Button>
        </FormWrapper>
      </form>
    )
  }

  _onSubmit = (e) => {
    e.preventDefault()
    this.props.createChannel(this.state)
  }

  _onUpdate = (e) => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value
    })
  }
}

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    createChannel: (data) => dispatch(createChannel(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateChannel)
