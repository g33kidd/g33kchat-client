export const SIGNUP_REQUEST = 'auth/SIGNUP_REQUEST'
export const SIGNUP_SUCCESS = 'auth/SIGNUP_SUCCESS'
export const SIGNUP_ERROR = 'auth/SIGNUP_ERROR'

export const LOGIN_REQUEST = 'auth/LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS'
export const LOGIN_ERROR = 'auth/LOGIN_ERROR'

export const SET_USER = 'auth/SET_USER'
export const SET_TOKEN = 'auth/SET_TOKEN'

export const LOGOUT = 'auth/LOGOUT'

export const VERIFY_REQUEST = 'auth/VERIFY_REQUEST'
