export const CALL_SERVER = 'CALL_SERVER'
export const CALL_SOCKET = 'CALL_SOCKET'

export const JOIN = 'JOIN'
export const LEAVE = 'LEAVE'

export const RECEIVE_OK = 'RECEIVE_OK'
export const RECEIVE_ERROR = 'RECEIVE_ERROR'
export const RECEIVE_TIMEOUT = 'RECEIVE_TIMEOUT'
