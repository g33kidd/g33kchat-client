// TODO: move to Utils file
const event = (type, payload) => {
  return {
    type,
    payload
  }
}

export default {
  'message:new': ({ state, dispatch, socket, data, topic }) => {
    let channel = topic.split(':')[1]    
    return event('message:new', {
      data,
      channel
    })
  },

  'shout': ({ state, dispatch, socket, data, topic }) => {
    // TODO: implement `topic_data` in middleware to get rid of this.
    let channel = topic.split(':')[1]
    return event('shout', {
      data,
      channel
    })
  },

  'ping': ({state, dispatch, socket, data}) => {
    return event('ping', data)
  },

  'pong': ({state, dispatch, socket, data}) => {
    return event('pong', data)
  }
}
