import { CALL_SERVER } from 'const/middleware'
import axios from 'axios'

export default function createServerMiddleware() {
  const middleware = store => next => action => {

    if (typeof action[CALL_SERVER] === 'undefined') {
      return next(action)
    }

    const token = localStorage.getItem('token') || null
    // const token = store.getState().auth.token || null
    const request = axios.create({
      baseURL: 'http://localhost:4000/api',
      headers: {
        'Authorization': token
      }
    })

    // PROBABLY NOT A GOOD FIX
    if (action.type) {
      store.dispatch({ type: action.type })
    }

    const {
      route,
      data,
      method,
      type,
      error_type,
      after,
      after_error,
      auth
    } = action[CALL_SERVER]

    return request({
      method,
      data,
      url: route
    }).then(response => {
      next({ type, payload: response.data })
      if(after) {
        store.dispatch(after())
      }
      return response
    }).catch(err => {
      next({ type: error_type, payload: err })
      if(after_error) {
        store.dispatch(after_error())
      }
      return err
    })
  }

  return middleware
}
