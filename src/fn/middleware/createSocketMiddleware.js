import { CALL_SERVER, CALL_SOCKET, JOIN } from 'const/middleware'

import axios from 'axios'

// Socket listeners should return an action for a reducer to handle
// the data.

export default function createSocketMiddleware({ socket }) {
  const middleware = store => next => action => {
    console.log(action)

    if (typeof action[JOIN] !== 'undefined') {
      const { topic, data, listener } = action[JOIN]
      const channel = socket.channel(topic, data)

      // Topic data separated by : and can be string or array.
      // {
      //   topic: 'channel',
      //   topic_data: ['g33kchat']
      // }
      // TODO :also clean this up

      for(let event in listener) {
        channel.on(event, socket_data => {
          let nextAction = listener[event]({
            state: store.getState(),
            dispatch: store.dispatch,
            socket,
            data: socket_data,
            topic
          })

          store.dispatch(nextAction)
        })
      }

      channel.join()
        .receive("ok", (data) => console.log('connected', data))
        .receive("error", ({reason}) => console.log('failed to join', reason))
        .receive('timeout', () => console.log("Networking issue... Still waiting..."))

      return channel
    }

    if (typeof action[CALL_SOCKET] !== 'undefined') {
      const { topic, event, data } = action[CALL_SOCKET]
      const channel = socket.channels.find(c => c.topic === topic)
      console.log(channel)
      channel.push(event, data)
      console.log(channel)
      return channel
    }

    return next(action)
  }

  return middleware
}
