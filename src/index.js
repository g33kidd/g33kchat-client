import { ConnectedRouter as Router, routerMiddleware, routerReducer } from 'react-router-redux'
import { applyMiddleware, combineReducers, compose, createStore } from 'redux'

import App from 'components/app'
import { Provider } from 'react-redux'
import React from 'react';
import ReactDOM from 'react-dom';
import { Socket } from 'phoenix'
import createHistory from 'history/createBrowserHistory'
import { injectGlobal } from 'styled-components'
import reducers from 'reducers'
import serverMiddleware from 'fn/middleware/createServerMiddleware'
import socketMiddleware from 'fn/middleware/createSocketMiddleware'
import thunk from 'redux-thunk'
import { verify } from 'actions/auth'

require('lato-font/css/lato-font.css')

const socketParams = {
  token: localStorage.getItem('token')
}
const socket = new Socket('ws://localhost:4000/socket', {params: socketParams})
socket.onMessage(console.log)
socket.connect()

const enhance = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const history = createHistory()
const historyMiddleware = routerMiddleware(history)
const middleware = applyMiddleware(
  historyMiddleware,
  thunk,
  serverMiddleware(),
  socketMiddleware({ socket })
)

const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer
  }),
  enhance(middleware)
)

store.dispatch(verify())

const Root = () => (
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'));

injectGlobal`
  html, body, #root {
    height: 100%;
    padding: 0;
    margin: 0
  }

  body {
    font-family: 'Lato', sans-serif;
    background: #eee;
  }
`
