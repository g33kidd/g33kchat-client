import {
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
  SET_TOKEN,
  SET_USER,
  SIGNUP_ERROR,
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  VERIFY_REQUEST
} from 'const/auth'

const initialState = {
  token: null,
  user: null,
  isLoggedIn: false,
  loginLoading: false,
  signupLoading: false,
  signupErrors: [],
  loginErrors: []
}

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loginLoading: true
      }
    case SIGNUP_REQUEST:
      return {
        ...state,
        signupLoading: true
      }  
    case SIGNUP_SUCCESS:
      localStorage.setItem('token', action.payload.token)  
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        signupLoading: false,
        isLoggedIn: true
      }
    case LOGIN_SUCCESS:
      localStorage.setItem('token', action.payload.token)  
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        loginLoading: false,
        isLoggedIn: true
      }
    case LOGIN_ERROR:
      return {
        ...state,
        loginErrors: action.payload
      }
    case SIGNUP_ERROR:
      return {
        ...state,
        signupErrors: action.payload
      }
    case LOGOUT:
      localStorage.removeItem('token')
      return initialState  
    default:
      return state 
  }
}

export default auth