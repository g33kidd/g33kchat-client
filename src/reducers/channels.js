import {
  CREATE_CHANNEL,
  CREATE_CHANNEL_ERROR,
  CREATE_CHANNEL_SUCCESS,
  FETCH_CHANNEL,
  FETCH_CHANNELS,
  FETCH_CHANNELS_ERROR,
  FETCH_CHANNELS_SUCCESS,
  FETCH_CHANNEL_ERROR,
  FETCH_CHANNEL_SUCCESS,
  MESSAGE_NEW,
  PING,
  PONG,
  SET_CURRENT,
  SHOUT
} from 'const/channels'

import _ from 'lodash'

const initialState = {
  raw: {},
  sortedByName: [],
  current: null,
  creating: false
}

// TODO: case for creating a channel and adding it to the raw dataset, then re-sort the list
const sortedChannels = (state) => {
  let keys = _.keys(state.raw)
  return keys.sort()
}

const mapRaw = (state, data) => {
  let rawChannels = {}
  _.each(data, c => rawChannels[c.identifier] = c)
  return rawChannels
}

const addChannels = (state, data) => {
  let raw = mapRaw(state, data)
  let sortedByName = sortedChannels({ raw })
  
  return {
    ...state,
    raw,
    sortedByName
  }
}

const addChannel = (state, data) => {
  let newState = {
    ...state,
    raw: {
      ...state.raw,
      [data.identifier]: data
    }
  }

  let sortedByName = sortedChannels(newState)

  return { ...newState, sortedByName }
}

const channels = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT:
      return { ...state, current: action.payload }
    case FETCH_CHANNELS_SUCCESS:
      return addChannels(state, action.payload)
    case FETCH_CHANNEL_SUCCESS:
      return addChannel(state, action.payload)
    case CREATE_CHANNEL_SUCCESS:
      return addChannel(state, action.payload)
    default:
      return state
  }
}

export default channels
