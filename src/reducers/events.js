import {
  MESSAGE_NEW,
  PING,
  PONG,
  SHOUT
} from 'const/channels'

const initialState = {
  'lobby': []
}

const addEvent = (state, { channel, data }) => {
  const existing = state[channel] || []
  return {
    ...state,
    [channel]: [...existing, data]
  }
}

const events = (state = initialState, action) => {
  switch (action.type) {
    case MESSAGE_NEW:
      return addEvent(state, action.payload)
    case SHOUT:
      return addEvent(state, action.payload) 
    default:
      return state  
  }
}

export default events