import auth from './auth'
import channels from './channels'
import events from './events'

export default {
  auth,
  channels,
  events
}
