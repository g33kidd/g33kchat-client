import colors from './colors'
import input from './input'

export {
  colors,
  input
}
