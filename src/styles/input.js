import styled from 'styled-components'

export const FormWrapper = styled.div`
  display: flex;
  padding: 20px;
  flex-direction: column;
`
export const FormHeader = styled.h1`
  font-size: 18px;
  color: #333;
`
export const Input = styled.input`
  padding: 10px;
  border: 2px solid #f4f7f7;
  border-radius: 3px;
  margin-top: 5px;
  font-size: 16px;
  transition: border-color 150ms ease;

  &:first-of-type {
    margin-top: 0;
  }

  &:focus {
    border-color: #d7dddd;
  }
`
export const Button = styled.button`
  margin-top: 10px;
  border: 0;
  padding: 10px;
  font-size: 16px;
  border-radius: 3px;
  background: #ff4e50;
  color: #FFF;
  text-transform: uppercase;
  transition: background 150ms ease;
  cursor: pointer;

  &:hover {
    background: #333;
  }
`
